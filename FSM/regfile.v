module regfile(input clk,input[2:0] rad1,input[2:0] rad2,
			input[2:0] wad, input wen, input[31:0] wdat,
			output[31:0] rdat1, output[31:0] rdat2);

reg[31:0] mem0,mem1,mem2,mem3,mem4,mem5,mem6,mem7;
wire[31:0] wrdat1, wrdat2;

always@(posedge clk)
begin
	if(wen==1'd1)
	begin
		case(wad)
			3'd0: mem0 <= wdat;
			3'd1: mem1 <= wdat;
			3'd2: mem2 <= wdat;
			3'd3: mem3 <= wdat;
			3'd4: mem4 <= wdat;
			3'd5: mem5 <= wdat;
			3'd6: mem6 <= wdat;
			3'd7: mem7 <= wdat;
		endcase
	end
end

always@(*)
begin
	case(rad1)
		3'd0: wrdat1 <= mem0;
		3'd1: wrdat1 <= mem1;
		3'd2: wrdat1 <= mem2;
		3'd3: wrdat1 <= mem3;
		3'd4: wrdat1 <= mem4;
		3'd5: wrdat1 <= mem5;
		3'd6: wrdat1 <= mem6;
		3'd7: wrdat1 <= mem7;
	endcase
end

always@(*)
begin
	case(rad2)
		3'd0: wrdat2 <= mem0;
		3'd1: wrdat2 <= mem1;
		3'd2: wrdat2 <= mem2;
		3'd3: wrdat2 <= mem3;
		3'd4: wrdat2 <= mem4;
		3'd5: wrdat2 <= mem5;
		3'd6: wrdat2 <= mem6;
		3'd7: wrdat2 <= mem7;
	endcase
end

assign rdat1 = wrdat1;
assign rdat2 = wrdat2;

endmodule


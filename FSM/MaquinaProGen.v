module MaquinaProGen(
input WEN,
input clk0,
input SEL,
input [31:0] OuterIn,
input [1:0] OP,
input [2:0] RAD2,
input [2:0] RAD1,
input [2:0] WAD,
output [31:0] dat1,
output iZ,
output [31:0] r,
output [31:0] dat2
);

/* Declarations (registers and wires) */
reg [31:0] mux0out;

/* Always blocks */
always @ (*) begin
  case (SEL)
    1'h0: mux0out = OuterIn;
    1'h1: mux0out = r;
    default: mux0out = 32'hx;
  endcase
end

/* Instances and always blocks */
/* alu
 */
alu alu0(
  .dt1(dat1),
  .op(OP),
  .dt2(dat2),
  .r(r),
  .z(iZ)
);
/* regfile
 */
regfile regfile0(
  .wen(WEN),
  .clk(clk0),
  .wdat(mux0out),
  .rad2(RAD2),
  .rad1(RAD1),
  .wad(WAD),
  .rdat2(dat2),
  .rdat1(dat1)
);

endmodule

module factorial(input clk1, input rst,input[31:0] N, output[31:0] F);
	reg[4:0] cs, ns;
	reg[31:0] outer, re;
	reg[31:0] c;
	wire wen, sel, iz;
	reg[3:0] wad, rad1, rad2;
	wire[1:0] op;

always@(cs or c or N)
begin
case(cs)
5'd0: 
	begin
		ns=5'd9;
		wad=4'd0;
		outer=32'd1;
		wen=1'd1;
		sel=1'd0;
	end
5'd1: 
	begin
		ns=5'd10;
		wad=4'd1;
		outer=32'd1;
		wen=1'd1;
		sel=1'd0;
	end
5'd2: 
	begin
		ns=5'd11;
		wad=4'd2;
		outer=32'd1;
		wen=1'd1;
		sel=1'd0;
	end
5'd3: 
	begin
		ns=5'd4;
		rad1=4'd2;
		rad2=4'd1;
		op=2'd2;
		wen=1'd0;
		sel=1'd0;
	end
5'd4: 
	begin
		ns=5'd5;
		rad1=4'd2;
		rad2=4'd1;
		op=2'd2;
		wen=1'd0;
		sel=1'd0;
	end
5'd5: 
	begin
		if(c<=N)
		begin
			ns = 5'd5;
		end else begin
			ns = 5'd8;
		end
	end
5'd6: 
	begin
		ns=5'd12;
		rad1=4'd0;
		rad2=4'd2;
		op=2'd2;
		wen=1'd1;
		sel=1'd1;
		wad=4'd0;
	end
5'd7: 
	begin
		ns=5'd13;
		rad1=4'd2;
		rad2=4'd1;
		op=2'd0;
		wen=1'd1;
		sel=1'd1;
		wad=4'd2;
	end
5'd8: 
	begin
		ns=5'd8;
		rad1=4'd0;
		rad2=4'd1;
		op=2'd2;
		wen=1'd0;
		sel=1'd1;
	end
5'd9: 
	begin
		ns=5'd1;
		wen=1'd0;
	end
5'd10: 
	begin
		ns=5'd2;
		wen=1'd0;
	end
5'd11: 
	begin
		ns=5'd3;
		wen=1'd0;
	end
5'd12: 
	begin
		ns=5'd7;
		wen=1'd0;
	end
5'd13: 
	begin
		ns=5'd4;
		wen=1'd0;
	end							
endcase
end

MaquinaProGen mpg(
			.WEN(wen),
			.clk0(clk1),
			.SEL(sel),
			.OuterIn(outer),
			.OP(op),
			.RAD2(rad2),
			.RAD1(rad1),
			.WAD(wad),
			.iZ(iz),
			.r(re)
		);

always@(posedge clk1)
begin
	if(cs==4'd4)
	begin
		c=re;
	end
	if (rst) 
		begin
			cs <= 32'd0;
		end
	else 
		begin
			cs <= ns;
		end
end

endmodule

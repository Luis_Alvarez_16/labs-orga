module alu(input[31:0] dt1, input[31:0] dt2, input[1:0] op,
		output z, output[31:0] r);

wire[31:0] wres;
wire isz;

always@(*)
begin
	case(op)
		2'd0: wres = dt1 + dt2;
		2'd1: wres = dt1 - dt2;
		2'd2: wres = dt1 * dt2;
		2'd3: wres = dt1 / dt2;
	endcase
	if(wres==32'd0)
	begin
		isz = 1'd1;
	end
end

assign z = isz;
assign r = wres;

endmodule

module romfibo(input[3:0] adr, output[54:0] ins);

reg[54:0] inst[0:10];

assign ins = inst[adr];


initial begin
  $readmemb("fibo.mif", inst,0,10);
end

endmodule

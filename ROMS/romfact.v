module romfact(input[3:0] adr, output[54:0] ins);

reg[54:0] inst[0:9];

assign ins = inst[adr];


initial begin
  $readmemb("fact.mif", inst,0,9);
end

endmodule
